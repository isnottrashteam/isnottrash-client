/**
 * Created by Bruno Rafal on 03/04/2016.
 */
app.directive('ngInsert', function () {
  return {
    scope: {
      callback: "="
    },
    link: function(scope,element, attrs) {
      var el = element;
      var callback = scope.callback;
      el.bind("keydown keypress keyup", function() {
        callback(el.val());
      });
    }
  };
});


