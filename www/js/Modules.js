/**
 * Created by Bruno Rafael on 08/02/2016.
 */

var app = angular.module('starter', ['ionic', 'ngCordova', 'starter.controllers', 'starter.services', 'pascalprecht.translate',
  'monospaced.elastic', 'angularMoment', 'angular-clipboard']);
var constants = angular.module('starter.constants', []);
var services = angular.module('starter.services', ['starter.constants']);
var controllers = angular.module('starter.controllers', ['starter.constants','jett.ionic.filter.bar', 'ngImgCrop', 'toastr']);
