/**
 * Created by Bruno Rafal on 17/02/2016.
 */
/**
 * The chat connection function -  called always that the user is connected
 * @param token{String} - The user logged token(today is the phone)
 *
 * Created by Bruno Rafael on 19/10/2015.
 */

(function(){

  services.service('WebSocketService',
      function(Socket){

        var vm = this;

        vm.connect = function(token){
          Socket.connect(token);
        };

        vm.registerListener = function(eventName, callback){
          Socket.on(eventName, callback);
        };

        vm.removeListener = function(eventName){
          Socket.removeListener(eventName);
        };

        vm.emit = function(eventName, json){
          Socket.emit(eventName, json);
        }
      }
  );
})();
