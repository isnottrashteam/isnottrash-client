/**
 * Created by Bruno Rafal on 30/04/2016.
 */
(function(){
  services.service('PromotionSocket',
    function($rootScope, WebSocketService, EVENTS){

        var vm = this;

        vm.addLike = function(promotion_id, resolve, reject){
            var json = {
              token: $rootScope.loggedUser.token,
              promotion_id: promotion_id
            };
            WebSocketService.emit(EVENTS.ADD_LIKE, json);
            function callback(resp){
              WebSocketService.removeListener(EVENTS.LIKE_SUCCESS);
              if(resp.success){
                resolve(resp.data);
              } else{
                reject(resp.data);
              }
            }
            WebSocketService.registerListener(EVENTS.LIKE_SUCCESS, callback);
        };

        vm.removeLike = function(promotion_id, resolve, reject){
            var json = {
              token: $rootScope.loggedUser.token,
              promotion_id: promotion_id
            };

            WebSocketService.emit(EVENTS.REMOVE_LIKE, json);

            function callback(resp){
              WebSocketService.removeListener(EVENTS.LIKE_SUCCESS);
              if(resp.success){
                resolve(resp.data);
              } else{
                reject(resp.data);
              }
            }

            WebSocketService.registerListener(EVENTS.LIKE_SUCCESS, callback);
        };

        vm.addComment = function(comment, token, resolve, reject){
            var cmtCopy = angular.copy(comment);
            cmtCopy._user = comment._user._id;
            delete cmtCopy._user.token;

            var sendComment = {
              token : token,
              comment: cmtCopy
            };

            WebSocketService.emit(EVENTS.ADD_COMMENT, sendComment);

            var newCommentResponse = EVENTS.ADD_COMMENT + EVENTS.RESPONSE_SUFIX + comment._user._id;
            WebSocketService.registerListener(newCommentResponse, function (resp){
                WebSocketService.removeListener(newCommentResponse);
                if(resp.success){
                    resolve(resp.data);
                } else{
                    reject(resp.data);
                }
            });
        };

        vm.removeComment = function(token, comment, resolve, reject){
            var json = {
              comment_id: comment._id,
              promotion_id: comment._promotion._id,
              token: token
            };

            WebSocketService.emit(EVENTS.REMOVE_COMMENT, json);
            var listener = EVENTS.REMOVE_COMMENT + EVENTS.RESPONSE_SUFIX + comment._id;
            function callback(resp){
              WebSocketService.removeListener(listener);
              if(resp.success){
                resolve(resp.data);
              } else{
                reject(resp.data);
              }
            }

            WebSocketService.registerListener(listener, callback);
        };

        vm.updateComment = function(comment, token){
            var json = {
              comment_id: comment._id,
              promotion_id: comment._promotion._id,
              newText: comment.text,
              token: token
            };

            WebSocketService.emit(EVENTS.UPDATE_COMMENT, json);
            var listener = EVENTS.UPDATE_COMMENT + EVENTS.RESPONSE_SUFIX + comment._id;
            function callback(resp){
              WebSocketService.removeListener(listener);
            }
            WebSocketService.registerListener(listener, callback);
        };

        vm.addUpdateLikesListener = function(callback){
            WebSocketService.registerListener(EVENTS.UPDATE_LIKES,
              function (json) {
                callback(json);
              }
            );
        };

        vm.registerNewCommentsAddedListener = function(promotion_id, callback){
            var newCommentId = EVENTS.NEW_COMMENT_ADDED + promotion_id;
            WebSocketService.registerListener(newCommentId, callback);
        };

        vm.registerCommentsRemovedListener = function(promotion_id, callback){
            var newCommentId = EVENTS.REMOVED_COMMENT + promotion_id;
            WebSocketService.registerListener(newCommentId, callback);
        };

        vm.registerCommentsUpdatedListener = function(promotion_id, callback){
            var newCommentId = EVENTS.UPDATED_COMMENT + promotion_id;
            WebSocketService.registerListener(newCommentId, callback);
        };

        vm.registerEstablishmentRankUpdate = function(callback){
            WebSocketService.registerListener(EVENTS.UPDATE_ESTABLISHMENT_RANK, callback);
        };

        vm.removeCommentsRemovedListener = function(promotion_id){
            var newCommentId = EVENTS.REMOVED_COMMENT + promotion_id;
            WebSocketService.removeListener(newCommentId);
        };

        vm.removeNewCommentsAddedListener = function(promotion_id){
            var newCommentListener = EVENTS.NEW_COMMENT_ADDED + promotion_id;
            WebSocketService.removeListener(newCommentListener);
        };

        vm.removeCommentsUpdatedListener = function(promotion_id){
            var newCommentId = EVENTS.UPDATED_COMMENT + promotion_id;
            WebSocketService.removeListener(newCommentId);
        };
    }
  );
})();
