/**
 * Created by Bruno Rafael on 20/02/2016.
 */

(function(){
  services.service('Socket',
      function(URL){

        var vm = this;

        vm.connect = function(token){

          vm.socket = io.connect(URL.SERVER_URL, {
            query: 'token=' + token
          });

          vm.socket.on('connect', function () {
            console.log('authenticated');
          }).on('disconnect', function () {
            console.log('disconnected');
          }).on('error', function () {
            console.log('error');
          }).on('reconnect', function () {
            console.log('reconnect');
          });


          vm.emit('init' , {data: "Cliente conectado!"},
            function(response){
              console.log(response);
            },
            function(response){
              console.log(response);
            }
          );
        };

        vm.emit = function(eventName, json, resolve, reject) {
          vm.socket.emit(eventName, json, function (json) {
            if(json.error){
              reject(json.error);
            } else {
              resolve(json);
            }
          });
        };

        vm.on = function(eventName, resolve, reject) {
          vm.socket.on(eventName, function (json) {
            if(json) {
              if (json.error) {
                if(reject) {
                  reject(json.error);
                }
              } else {
                if(resolve) {
                  resolve(json);
                }
              }
            }
          });
        };

        vm.removeListener = function(listenerName){
          vm.socket.removeListener(listenerName);
        }

      });
})();

