/**
 * Created by Bruno Rafal on 10/06/2016.
 */
(function() {
  services.service('NotificationSocket',
    function ($rootScope, $state, WebSocketService, EVENTS) {
      var vm = this;

      vm.registerEvents = function (){
        WebSocketService.registerListener(EVENTS.NOTIFICATION_ADDED, function(response){
          $rootScope.$broadcast(EVENTS.NOTIFICATION_ADDED, [response]);
        });
      }
    }
  );
}
)();
