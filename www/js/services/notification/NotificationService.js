/**
 * Created by Bruno Rafael on 03/04/2016.
 */
(function() {
  services.service('NotificationService', function($rootScope, $http, URL){
    var vm = this;

    vm.removeNotification = function() {
      return $http.delete();
    };

    vm.loadOldNotifications = function(params){
      return $http.post(URL.LOAD_OLD_NOTIFICATIONS, params);
    };

    vm.getNotificationsNotVisualizedCount = function(token){
      return $http.post(URL.VISUALIZED_COUNT, {token: token});
    };

    vm.visualizeNotifications = function(params){
      return $http.put(URL.VISUALIZE_NOTIFICATION, params);
    };
  });

})();
