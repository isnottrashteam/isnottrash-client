/**
 * Created by Bruno Rafal on 10/07/2016.
 */
(function(){
  constants.constant('EVENTS',{
    NOTIFICATION_ADDED: 'notificationAdded',
    ADD_LIKE: 'addLike',
    REMOVE_LIKE: 'removeLike',
    STATE_CHANGE_SUCCESS: '$stateChangeSuccess',
    ADD_COMMENT: 'addComment',
    REMOVE_COMMENT: 'removeComment',
    UPDATE_COMMENT: 'updateComment',
    UPDATE_LIKES: 'updateLikes',
    NEW_COMMENT_ADDED: 'newCommentAdded',
    REMOVED_COMMENT: 'commentRemoved',
    UPDATED_COMMENT: 'updatedComment',
    LIKE_SUCCESS: 'likeSuccess',
    RESPONSE_SUFIX: 'Response',
    NOTIFICATION_UPDATED : "notificationsUpdated",
    UPDATE_ESTABLISHMENT_RANK: 'updateEstablishmentRank'
  });
})();
