/**
 * Created by Bruno Rafael on 16/02/2016.
 */
(function(){

    //const SERVER_URL = 'http://localhost:1337',
    const SERVER_URL = 'http://34.199.124.116:1337',
          SERVER_API =  SERVER_URL + '/api',
          PROMOTIONS_PREFIX = SERVER_API + '/promotions',
          ESTABLISHMENTS_PREFIX = SERVER_API + '/establishments',
          FAVORITE_PREFIX = SERVER_API + '/favorite',
          USERS_PREFIX = SERVER_API + "/users",
          LOGIN_PREFIX = SERVER_API + '/login',
          NOTIFICATION_PREFIX = SERVER_API + '/notification';

    constants.constant('URL',
    {
        SERVER_URL : SERVER_URL,
        SERVER_API : SERVER_API,
        PROMOTIONS_PREFIX: PROMOTIONS_PREFIX,
        LOGIN: LOGIN_PREFIX,
        REGISTER_USER_URL : LOGIN_PREFIX + '/create/user',
        OLD_PROMOTIONS: PROMOTIONS_PREFIX + '/oldPromotions',
        NEW_PROMOTIONS: PROMOTIONS_PREFIX + '/newPromotions',
        OLD_COMMENTS: PROMOTIONS_PREFIX + '/oldComments',
        NEW_COMMENTS: PROMOTIONS_PREFIX + '/newComments',
        RECOVER_PASSWORD: SERVER_API + '/recoverPassword',
        PIN_CODE: SERVER_API + '/pinCode',
        ADD_COMMENT: PROMOTIONS_PREFIX + '/addComment',
        ADD_FAVORITE: FAVORITE_PREFIX,
        REMOVE_FAVORITE: FAVORITE_PREFIX,
        PRODUCT_HISTORY: PROMOTIONS_PREFIX + "/history",
        SEARCH_ESTABLISHMENTS_BY_NAME: ESTABLISHMENTS_PREFIX + "/search/name",
        SEARCH_PROMOTIONS_BY_NAME: PROMOTIONS_PREFIX + "/search/name",
        UPDATE_EMAIL: USERS_PREFIX + "/email",
        UPDATE_NAME: USERS_PREFIX + "/name",
        UPDATE_PASSWORD: USERS_PREFIX + "/password",
        UPDATE_LANGUAGE: USERS_PREFIX + "/language",
        UPDATE_PHONE: USERS_PREFIX + "/phone",
        UPDATE_PHOTO: USERS_PREFIX + "/photo",
        UPDATE_FINALIZED_PROMOTIONS: USERS_PREFIX + "/finalized/promotions",
        UPDATE_VIBRATION_SETTING: USERS_PREFIX + "/vibration",
        LOAD_OLD_NOTIFICATIONS: NOTIFICATION_PREFIX + "/old",
        VISUALIZE_NOTIFICATION: NOTIFICATION_PREFIX + "/visualize/all",
        VISUALIZED_COUNT: NOTIFICATION_PREFIX + "/visualized/count",
        REMOVE_NOTIFICATION: PROMOTIONS_PREFIX + "/notification/remove"

  });
})();
