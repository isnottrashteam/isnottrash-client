/**
 * Created by Bruno Rafal on 13/07/2016.
 */
(function() {
  services.service('FavoriteService',
      function (URL, $http) {
        var vm = this;

        vm.addFavorite = function(token, promotionId, productType){
          return $http.post(URL.ADD_FAVORITE, {promotionId: promotionId, name : productType, token: token});
        };

        vm.removeFavorite = function(token, promotionId){
          return $http.delete(URL.ADD_FAVORITE, {params: {promotionId: promotionId, token: token}});
        }
      }
  )
})();
