/**
 * Created by Bruno Rafal on 13/02/2016.
 */

(function(){

  services.service('UserService',
    ['$http', 'URL',
      function($http, URL) {

        var vm = this;

        vm.createUser = function(json){
          return $http.post(URL.REGISTER_USER_URL, json);
        };

        vm.getUsers = function(){
          return $http.get(URL.REGISTER_USER_URL);
        };

        vm.login = function(json){
          return $http.post(URL.LOGIN,json);
        };

        vm.recoverPass = function(email){
          return $http.post(URL.RECOVER_PASSWORD, email);
        };

        vm.pinCode = function(pinCode) {
          return $http.post(URL.PIN_CODE, pinCode);
        }

        vm.updateEmail = function(id,email,token){
          var params = {
            _id: id,
            email: email,
            token: token
          };
          return $http.put(URL.UPDATE_EMAIL, params);
        };

        vm.updateFinalizedPromotions = function(removeFinishPromotions, token){
          var json = {
            removeFinishPromotions: removeFinishPromotions,
            token: token
          };
          return $http.put(URL.UPDATE_FINALIZED_PROMOTIONS, json);
        };

        vm.updateVibration = function(vibration, token){
          var params = {
            token: token,
            vibration: vibration
          };

          return $http.put(URL.UPDATE_VIBRATION_SETTING, params);
        };

        vm.updateName = function(id,name,token){
          var params = {
            _id: id,
            name: name,
            token: token
          };
          return $http.put(URL.UPDATE_NAME, params);
        };

        vm.updateLanguage = function(id,language,token){
          var params = {
            _id: id,
            language: language,
            token: token
          };
          return $http.put(URL.UPDATE_LANGUAGE, params);
        };

        vm.updatePassword = function(id,password,newPassword,token){
          var params = {
            _id: id,
            password: password,
            newPassword: newPassword,
            token: token
          };
          return $http.put(URL.UPDATE_PASSWORD, params);
        };

        vm.updatePhone = function(id,phone,token){
          var params = {
            _id: id,
            phone: phone,
            token: token
          };
          return $http.put(URL.UPDATE_PHONE, params);
        };

        vm.updatePhoto = function(id,photo,token){
          var params = {
            _id: id,
            photo: photo,
            token: token
          };
          return $http.put(URL.UPDATE_PHOTO, params);
        };
      }
    ]
  );
})();
