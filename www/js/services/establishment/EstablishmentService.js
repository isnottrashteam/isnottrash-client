/**
 * Created by Bruno Rafael on 03/04/2016.
 */
(function(){
  services.service('EstablishmentService',
    ['URL', '$http',
      function(URL, $http){
        var vm = this;

        vm.RANK = {
          Bronze: {
            class: 'bronze-color',
            icon: 'ion-ribbon-b'
          },
          Silver: {
            class: 'silver-color',
            icon: 'ion-ribbon-b'
          },
          Gold: {
            class: 'gold-color',
            icon: 'ion-ribbon-b'
          },
          Diamond: {
            class: "diamond-color",
            icon: ''
          }
        };

        vm.searchById = function(id, token){
          var params = {params:{token: token, id: id}};
          return $http.get(URL.SEARCH_ESTABLISHMENTS, params);
        };

        vm.searchByName = function(name, token){
          var params = {params:{token: token, name: name}};
          return $http.get(URL.SEARCH_ESTABLISHMENTS_BY_NAME, params);
        };

        vm.searchEstablishmentPromotions = function(id, token, skip, limit){
          var params = {
            token: token,
            establishmentId: id,
            skip: skip,
            limit: limit
          };

          return $http.post(URL.OLD_PROMOTIONS, params);
        }
      }
    ]
  );
})();
