/**
 * Created by Bruno Rafal on 16/02/2016.
 */
(function(){
  services.service('PromotionService',
    ['$http', 'URL',
      function($http, URL){

        var vm = this;

        vm.loadOldPromotions = function(params){
          return $http.post(URL.OLD_PROMOTIONS, params);
        };

        vm.loadNewPromotions = function(params){
          return $http.post(URL.NEW_PROMOTIONS, params);
        };

        vm.loadOldComments = function(skip, limit, promotion_id, token){
          var params = {
            skip: skip,
            limit: limit,
            promotion_id: promotion_id,
            token: token
          };

          return $http.post(URL.OLD_COMMENTS, params);
        };

        vm.productHistory = function(skip, limit, productType, token) {
          var params = {
            productType: productType,
            skip: skip,
            limit: limit,
            token: token
          };

          return $http.post(URL.PRODUCT_HISTORY, params);
        };

        vm.loadNewComments = function(promotionId, lastCommentId, token){
          var params = {
            promotionId: promotionId,
            lastCommentId: lastCommentId,
            token: token
          };

          return $http.post(URL.NEW_COMMENTS, params);
        };

        vm.searchByName = function(token, name){
          if(name === undefined){
            return;
          }

          var params = {
            params:{
              token: token,
              name: name
            }
          };

          return $http.get(URL.SEARCH_PROMOTIONS_BY_NAME, params);
        };
      }
    ]
  );
})();
