/**
 * Created by Bruno Rafael on 04/02/2016.
 */
(function() {
  controllers.controller('LoginCtrl',
    ['$scope', '$state', '$rootScope', '$ionicPopup', '$translate', '$window',
    'WebSocketService', 'PromotionSocket', 'UserService',   'NotificationSocket', 'toastr',
      function($scope, $state, $rootScope, $ionicPopup, $translate, $window,
        WebSocketService, PromotionSocket, UserService, NotificationSocket, toastr){

        $scope.loginUser = {};

        $scope.image = {'src': 'img/icon.png'};

        $translate('ERR').then(function (err) {
          $scope.ERR = err;
        });
        $translate('ERR_LOGIN').then(function (err_login) {
          $scope.ERR_LOGIN = err_login;
        });
        $translate('ERR_CONNECTION').then(function (err_connection) {
          $scope.ERR_CONNECTION = err_connection;
        });
        $translate('ERR_AUTHENTICATION').then(function (err_authentication) {
          $scope.ERR_AUTHENTICATION = err_authentication;
        });
        $translate('INVALID_FIELDS').then(function (invalid_fields) {
          $scope.INVALID_FIELDS = invalid_fields;
        });
        $translate('INVALID_LOGIN').then(function(invalid_login) {
          $scope.INVALID_LOGIN = invalid_login;
        });

        $scope.login = function(loginForm,loginUser){
          if($scope.verificaCampos(loginForm)){
            toastr.info($scope.INVALID_FIELDS);
            //$ionicPopup.alert({title: $scope.INVALID_FIELDS});
          } else {
            $scope.authenticating = true;
            var user = {
              email:loginUser.login,
              password:loginUser.senha
            };
            UserService.login(user).then(function(result){
              if(result.data.success){
                /*if (document.getElementById("manterLogado").checked) {
                  localStorage.setItem('manterLogado',true);
                }*/
                var userData = result.data.userInformations;
                userData.token= result.data.token;

                $rootScope.sharedPreferences.getSharedPreferences('loggedUserPreferences', 'MODE_PRIVATE',
                  function(){
                    $rootScope.sharedPreferences.putString('usuarioLogado', JSON.stringify(userData));

                    $rootScope.loggedUser = userData;
                    $rootScope.myCroppedImage = $rootScope.loggedUser.photo;
                    $translate.use(userData.settings.language);
                    $state.go('tab.timeline');
                    $scope.authenticating = false;
                    WebSocketService.connect($rootScope.loggedUser.token);
                    NotificationSocket.registerEvents();

                  },
                  function(){
                    toastr.error($scope.INVALID_LOGIN, $scope.ERR_AUTHENTICATION );
                  }
                );

              } else {
                toastr.error($scope.INVALID_LOGIN, $scope.ERR_AUTHENTICATION );
                //$ionicPopup.alert({title: $scope.ERR_AUTHENTICATION, template: '<div class="text-center">{{"INVALID_LOGIN" | translate}}</div>'});
                $scope.authenticating = false;
              }
            }).catch( function(result){
              $scope.authenticating = false;
              if(!status && !result.data){
                toastr.error($scope.ERR_CONNECTION, $scope.ERR_AUTHENTICATION );
              } else {
                toastr.error($scope.ERR_LOGIN, $scope.ERR );
                //$ionicPopup.alert({title: $scope.ERR, template: '<div class="text-center">{{"ERR_LOGIN" | translate}}</div>'});

              }

            });
          }
        };

        $scope.verificaCampos = function(login) {
          return login.$invalid;
        };
      }
    ]
  );
})();
