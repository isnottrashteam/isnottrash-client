/**
 * Created by Bruno Rafal on 13/02/2016.
 */
(function(){
  controllers.controller('PopupCtrl',
    ['$scope', '$ionicPopup', '$ionicLoading', '$translate', 'UserService', 'toastr',

      function($scope, $ionicPopup, $ionicLoading, $translate, UserService, toastr){

        const MIN_PASSWORD_LENGTH = 6;

        $translate('CREATE_ACCOUNT').then(function (create_account) {
          $scope.CREATE_ACCOUNT = create_account;
        });
        $translate('USER_DATA').then(function (user_data) {
          $scope.USER_DATA = user_data;
        });
        $translate('USER_CONFIRM_EMAIL').then(function (user_confirm_email) {
          $scope.USER_CONFIRM_EMAIL = user_confirm_email;
        });
        $translate('USER_EMAIL_ENTRY').then(function (user_email_entry) {
          $scope.USER_EMAIL_ENTRY = user_email_entry;
        });
        $translate('CANCEL').then(function (cancel) {
          $scope.CANCEL = cancel;
        });
        $translate('SEND').then(function (send) {
          $scope.SEND = send;
        });
        $translate('INVALID_DATA').then(function (invalid_data) {
          $scope.INVALID_DATA = invalid_data;
        });
        $translate('ERR_CONNECTION').then(function (err_connection) {
          $scope.ERR_CONNECTION = err_connection;
        });
        $translate('USER_CREATE').then(function (user_create) {
          $scope.USER_CREATE = user_create;
        });
        $translate('USER_NOT_CREATE').then(function (user_not_create) {
          $scope.USER_NOT_CREATE = user_not_create;
        });
        $translate('CONFLIT_EMAIL').then(function (conflit_email) {
          $scope.CONFLIT_EMAIL = conflit_email;
        });
        $translate('ENTER_PIN_CODE').then(function (pincode) {
          $scope.ENTER_PIN_CODE = pincode;
        });
        $translate('REQUIRED_FIELD').then(function (rfield) {
          $scope.REQUIRED_FIELD = rfield;
        });
        $translate('INVALID_PIN').then(function (inv_pin) {
          $scope.INVALID_PIN = inv_pin;
        });
        $translate('CONFIRM_PSW').then(function (conf_psw) {
          $scope.CONFIRM_PSW = conf_psw;
        });
        $translate('PSW_RESET').then(function (psw_reset) {
          $scope.PSW_RESET = psw_reset;
        });
        $translate('PSW').then(function (psw_reset) {
          $scope.PSW = psw_reset;
        });
        $translate('PASS_DO_NOT_MATCH').then(function (psw_dont_match) {
          $scope.PASS_DO_NOT_MATCH = psw_dont_match;
        });

        $translate('PASSWORD_SIZE').then(function (psw_size) {
          $scope.PASSWORD_SIZE = psw_size;
        });

        var popup;
        $scope.createRegisterUserPopup = function(){
          $scope.newUser = {};
          popup = $ionicPopup.show({
            templateUrl: 'registerUserPopup.html',
            title: $scope.CREATE_ACCOUNT,
            subTitle: $scope.USER_DATA,
            scope: $scope
          });
        };

        $scope.createSendPasswordPopup = function() {
          $scope.info = {};
          $ionicPopup.show({
            template: '<input type="email" ng-model="info.email" type="email" placeholder="example@mail.com" required>',
            title: $scope.USER_CONFIRM_EMAIL,
            subTitle: $scope.USER_EMAIL_ENTRY,
            scope: $scope,
            buttons: [
              { text:  $scope.CANCEL },
              {
                text: '<b>' + $scope.SEND + '</b>',
                type: 'button-positive',
                onTap: function(e) {
                  if (!$scope.info.email) {
                    toastr.warning($scope.INVALID_DATA);
                    //$scope.showAlert($scope.INVALID_DATA);
                    e.preventDefault();
                  } else {
                    $scope.email = $scope.info.email;
                    $scope.sendEmail({'email': $scope.info.email});
                    $scope.verifyPinCodePopup();
                  }
                }
              }]
          });
        };

        $scope.redefinePsw = function() {
          $scope.info = {};
          $ionicPopup.show({
            template: '<input type="password" ng-model="info.password" type="text" placeholder="" required>'+
            '<p>'+$scope.CONFIRM_PSW+'</p>'+
            '<input type="password" ng-model="info.conf_password" type="text" placeholder="" required>',
            title: $scope.PSW_RESET,
            subTitle: $scope.PSW,
            scope: $scope,
            buttons: [
              { text:  $scope.CANCEL },
              {
                text: '<b>' + $scope.SEND + '</b>',
                type: 'button-positive',
                onTap: function(e) {
                  if (!$scope.info.password) {
                    toastr.warning($scope.REQUIRED_FIELD);
                    e.preventDefault();
                  } else if($scope.info.password != $scope.info.conf_password){
                    toastr.error($scope.PASS_DO_NOT_MATCH);
                    e.preventDefault();
                  } else if($scope.info.password.length < MIN_PASSWORD_LENGTH) {
                    toastr.error($scope.PASSWORD_SIZE);
                    e.preventDefault();
                  } else {

                  }
                }
              }]
          });
        };

        $scope.verifyPinCodePopup = function() {
          $scope.info = {};
          $ionicPopup.show({
            template: '<input type="text" ng-model="info.pin" type="text" placeholder="" required>',
            title: $scope.email,
            subTitle: $scope.ENTER_PIN_CODE,
            scope: $scope,
            buttons: [
              { text:  $scope.CANCEL },
              {
                text: '<b>' + $scope.SEND + '</b>',
                type: 'button-positive',
                onTap: function(e) {
                  if (!$scope.info.pin) {
                    toastr.warning($scope.REQUIRED_FIELD);
                    e.preventDefault();
                  } else {
                    $scope.pinCode({'pinCode': $scope.info.pin, 'email': $scope.email});
                    e.preventDefault();
                  }
                }
              }]
          });
        };

        $scope.pinCode = function(pincode) {
          UserService.pinCode(pincode).then(function(result) {
            if(result.data.content.success == true) {
              $scope.redefinePsw();
            } else {
              toastr.error($scope.INVALID_PIN);
            }
          }).catch(function(error) {
            toastr.error($scope.ERR_CONNECTION);
          });
        };

        $scope.sendEmail = function(email) {
          UserService.recoverPass(email).then(function(result){
            toastr.info(email);
          }).catch(function(erro){
            toastr.error($scope.ERR_CONNECTION);
            //$scope.showAlert($scope.ERR_CONNECTION);
          });
        };

        $scope.createUser = function(newUser) {
          $ionicLoading.show({
            template: '<ion-spinner icon="bubbles" class="spinner-positive"></ion-spinner>'
          });
          delete newUser.confirmPassword;
          UserService.createUser(newUser).then(function(result){
            $ionicLoading.hide();
            toastr.success($scope.USER_CREATE);
            //$scope.showAlert($scope.USER_CREATE);
          }).catch( function(exception){
            $ionicLoading.hide();
            var msg = "";
            if (exception.status == 400) {
              msg = $scope.USER_NOT_CREATE;
            } else {
              msg = $scope.CONFLIT_EMAIL;
            }
            toastr.error(msg);
            //$scope.showAlert(msg);
          });
          $scope.cancel();
        };

        $scope.cancel = function() {
          popup.close();
        };

        $scope.focus = function(campo) {
          campo.$focus=true;
          campo.$blur=false;
        };

        $scope.blur = function(campo) {
          campo.$focus=false;
          campo.$blur=true;
        };

        $scope.showAlert = function(message) {
          var alertPopup = $ionicPopup.alert({
            title: message,
          });
        };
      }
    ]
  );
})();
