/**
 * Created by Bruno Rafal on 08/02/2016.
 */
(function(){

  controllers.controller('HeaderCtrl',
  ['$scope', '$state', '$rootScope', '$ionicLoading', '$ionicHistory',
    '$timeout', '$translate', '$ionicPopup', '$window', 'UserService', 'toastr',
      function($scope, $state, $rootScope, $ionicLoading, $ionicHistory,
               $timeout, $translate, $ionicPopup, $window, UserService, toastr){

        $translate('ERR').then(function (err) {
          $scope.ERR = err;
        });
        $translate('ERR_LOGOUT').then(function (err_logout_message) {
          $scope.ERR_LOGOUT = err_logout_message;
        });

        $scope.logout = function() {
          $ionicLoading.show({
            template: '<ion-spinner icon="bubbles" class="spinner-positive"></ion-spinner>'
          });
          $timeout(function(){
            $rootScope.sharedPreferences.remove('usuarioLogado',
                function(){
                  $ionicLoading.hide();
                  $ionicHistory.clearCache();
                  $ionicHistory.clearHistory();
                  $ionicHistory.nextViewOptions({ disableBack: true, historyRoot: true });
                  $state.go('login');
                },
                function(){
                  toastr.error($scope.ERR, $scope.ERR_LOGOUT);
                }
            );
          }, 1000);
        };

        $scope.showSettings = function() {
          $state.go('settings');
        };

        // An elaborate, custom popup
        $scope.userPhoto = '';
        $scope.myImage='';

        var handleFileSelect=function(evt) {
          var file=evt.currentTarget.files[0];
          var reader = new FileReader();
          reader.onload = function (evt) {
            $scope.$apply(function($scope){
              $scope.myImage=evt.target.result;
            });
          };
          reader.readAsDataURL(file);
        };

        angular.element(document.querySelector('#fileInput')).on('change',handleFileSelect);

        $scope.updateUserPhoto = function() {
          $scope.data = {};

          // An elaborate, custom popup
          $translate(['UPDATE_PHOTO', 'CANCEL', 'SAVE']).then(function(translations) {

          $ionicPopup.show({

            title: translations.UPDATE_PHOTO,
            templateUrl: 'userUpdatePhoto.html',
            //scope: $scope,
            buttons: [
              { text: translations.CANCEL },
              {
                text: translations.SAVE,
                type: 'button-positive',
                onTap: function () {
                  $scope.userPhoto = document.getElementById("photo").src;
                  UserService.updatePhoto($rootScope.loggedUser._id, $scope.userPhoto, $rootScope.loggedUser.token).then(function(json){
                    if (json.status === 200) {
                      $rootScope.loggedUser.photo = json.data.user.photo;
                    } else {
                      $ionicPopup.alert({title: $scope.ERR, template: "{{'ERR_EDIT_EMAIL' | translate}}"});
                    }
                  }).catch(function(exception){
                    $ionicPopup.alert({title: $scope.ERR, template: "{{'ERR_CONNECTION' | translate}}"});
                  });
                }
              }
            ]
          })
        });
        };
        // A confirm dialog
        $scope.showConfirm = function() {
          var confirmPopup = $ionicPopup.confirm({
            title: 'Consume Ice Cream',
            template: 'Are you sure you want to eat this ice cream?'
          });

        confirmPopup.then(function(res) {
          if(res) {
            //console.log('You are sure');
          } else {
            //console.log('You are not sure');
          }
        });
      };
    }]
  );
})();
