/**
 * Created by Bruno Rafael on 09/06/2016.
 */

(function(){
  controllers.controller('TabsCtrl', function($scope, $rootScope, $state, NotificationService, STATES, EVENTS){
    $scope.badges = {
      notifications: 0,
      newPromotionsAdded: 0
    };

    function cleanNotifications(){
      $scope.badges.notifications = 0;
    }

    $scope.goToTimelineTab = function(){
      $rootScope.goTo('tab.timeline');
    };

    $scope.goToSearchTab = function(){
      $rootScope.goTo('tab.search');
    };

    $scope.goToNotificationTab = function(notifications, token){
      var ids = [];
      for(var i = 0; i < notifications.length; i++){
        if(!notifications[i].visualized){
          ids.push(notifications[i]._id);
        }
      }
      NotificationService.visualizeNotifications({ids: ids, token: token}).then(function (result) {
        if (result.data.success) {
          cleanNotifications();
        } else {

        }

      });
      $rootScope.goTo('tab.notification');
    };

    $rootScope.$on(EVENTS.NOTIFICATION_ADDED, function(json, array){
      if($state.current.name != STATES.NOTIFICATIONS) {
        $scope.badges.notifications += array.length;
        $scope.$applyAsync();
      }
    });

    $scope.$on(EVENTS.STATE_CHANGE_SUCCESS, function(event, toState){
      if(toState.name == STATES.NOTIFICATIONS){
        cleanNotifications();
      }
    });

    $rootScope.$on('newNotification', function(){
      $scope.badges.notifications++;
      $scope.$applyAsync();
    });

    $rootScope.$on('newPromotionsUpdated', function(num){
      $scope.badges.newPromotionsAdded -= num;
      $scope.$applyAsync();
    });

    NotificationService.getNotificationsNotVisualizedCount($rootScope.loggedUser.token).then(
      function(json){
        var response = json.data;
        if(response.success){
          $scope.badges.notifications = response.data;
        } else {
          //console.error("Erro ao recuperar total de notificações não visualizadas");
        }

      }
    );
  });
})();
