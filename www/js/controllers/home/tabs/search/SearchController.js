/**
 * Created by Bruno Rafal on 19/03/2016.
 */
(function(){
  controllers.controller('SearchCtrl',
    ['$rootScope', '$scope', '$ionicFilterBar', '$timeout', 'EstablishmentService', 'PromotionService',
      function($rootScope, $scope, $ionicFilterBar, $timeout, EstablishmentService, PromotionService){

        const DELAY = 800;

        $scope.ids = {
          SEARCH_ESTABLISH_BY_NAME : 'SEARCH_ESTABLISHMENT_BY_NAME',
          SEARCH_PROMOTION_BY_NAME : 'SEARCH_PROMOTION_BY_NAME'
        };

        var TOOGLE_CLASS = {
          SHOW : ['ion-ios-arrow-down', 'ng-hide-remove'],
          HIDE: ['ion-ios-arrow-forward', 'ng-hide-add']
        };

        var timeouts = {};

        $scope.data = {
          value: 'SEARCH_ESTABLISHMENT_BY_NAME'
        };

        $scope.searchOptions = {
          'SEARCH_ESTABLISHMENT_BY_NAME': {
            execute: function (name) {
              if (timeouts[$scope.ids.SEARCH_ESTABLISH_BY_NAME]) {
                $timeout.cancel(timeouts[$scope.ids.SEARCH_ESTABLISH_BY_NAME]);
              }

              timeouts[$scope.ids.SEARCH_ESTABLISH_BY_NAME] = $timeout(function () {
                EstablishmentService.searchByName(name, $rootScope.loggedUser.token).
                then(function (response) {
                  delete timeouts[$scope.ids.SEARCH_ESTABLISH_BY_NAME];
                  $scope.items = response.data.data;
                }).
                catch(function (exception) {
                  delete timeouts[$scope.ids.SEARCH_ESTABLISH_BY_NAME];
                //  console.log(exception);
                });
              }, DELAY);
            }
          },
          'SEARCH_PROMOTION_BY_NAME':{
            execute: function (name) {

              if(timeouts[$scope.ids.SEARCH_PROMOTION_BY_NAME]){
                $timeout.cancel(timeouts[$scope.ids.SEARCH_PROMOTION_BY_NAME]);
              }

              timeouts[$scope.ids.SEARCH_PROMOTION_BY_NAME] = $timeout(function () {
                PromotionService.searchByName($rootScope.loggedUser.token, name).
                then(function (response) {
                  delete timeouts[$scope.ids.SEARCH_PROMOTION_BY_NAME];
                  $scope.items = response.data.data;
                }).
                catch(function (exception) {
                  delete timeouts[$scope.ids.SEARCH_PROMOTION_BY_NAME];
                  //console.log(exception);
                });
              }, DELAY);
            }
          }
        };

        $scope.items = [];
        $scope.toogleClass = TOOGLE_CLASS.HIDE;
        $scope.actualMode = $scope.searchOptions[0];

        $scope.toggleGroup = function() {
          $scope.show = !$scope.show;
          $scope.toogleClass = $scope.show ?  [TOOGLE_CLASS.SHOW]: [TOOGLE_CLASS.HIDE];
        };

        $scope.showSearchConfig = function(){
          $scope.show = !$scope.show;
          $scope.search("");
        };

        $scope.search = function(name){
          var searchType = $scope.data.value;
          $scope.searchOptions[searchType].execute(name);
        };

        $scope.search("");
      }
    ]);
})();
