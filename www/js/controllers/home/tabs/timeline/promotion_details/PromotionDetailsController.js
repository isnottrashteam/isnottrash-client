(function() {
    controllers.controller('PromotionDetailsCtrl',
        ['$scope', '$state', '$stateParams', 'EstablishmentService', '$ionicPopup', '$timeout', '$translate', 'toastr', 'PromotionService',
            function($scope, $state, $stateParams, EstablishmentService, $ionicPopup, $timeout, $translate, toastr, PromotionService){
                $scope.promotion = $stateParams.item;

                $translate('ERR').then(function (err) {
                    $scope.ERR = err;
                });

                $scope.calculateDiscount = function(oldPrice, price){
                    var difference = oldPrice - price;
                    var percent = (100* difference) / oldPrice;
                    return Math.round(percent);
                };

                $scope.openEstablishment = function(establishment){
                    $state.go('establishment_perfil', {'establishment': establishment});
                };

                $scope.listdata = [];

                $scope.productHistory = function(skip, limit) {
                    var params = {
                        productType: $scope.promotion.productType,
                        skip: skip,
                        limit: limit
                    };

                    PromotionService.productHistory(params).then(
                        function (response) {
                            if (reponse && response.length > 0) {
                                $scope.moreData = true;

                                //var item = response.data[index];
                                $scope.listData = response.data;


                                $scope.$broadcast('scroll.infiniteScrollComplete');
                            } else {
                                $scope.moreData = false;
                            }
                        }
                    ).catch(function () {
                        $scope.moreData = false;
                        $toastr.error($scope.ERR_CONNECTION, $scope.ERR);
                        //$ionicPopup.alert({title: $scope.ERR, template: "{{'ERR_CONNECTION' | translate}}"});
                    });
                };

                $translate(['CANCEL']).then(function(translations) {
                    $scope.showList = function(){
                        var listPopup = $ionicPopup.show({
                            templateUrl: "productHistory.html",
                            title: $scope.promotion.productName,
                            scope: $scope,
                            buttons: [
                                { text: translations.CANCEL, type: 'button-positive' }
                            ]
                        });

                    }
                })
            }
        ]
    );
})();
