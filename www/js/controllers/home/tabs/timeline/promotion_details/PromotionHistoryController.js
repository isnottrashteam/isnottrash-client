/**
 * Created by Bruno Rafal on 26/07/2016.
 */
(function(){
  controllers.controller('PromotionHistoryController', function($rootScope, $scope,
                                                                $ionicPopup, $translate, PromotionService){

    $scope.history = [];
    $scope.moreHistory = true;

    $scope.showPromotionHistory = function(){
      return $ionicPopup.show({
        templateUrl: "promotion-history.html",
        title: $scope.promotion.productName,
        scope: $scope,
        cssClass: 'history-promotion-popup',
        buttons: [
          { text: $translate.instant('CLOSE'), type: 'button-positive' }
        ]
      });
    };

    $scope.getPromotionTypeHistory = function(skip, limit, productType, token) {
      PromotionService.productHistory(skip, limit, productType, token).then(
        function (response) {
          if (response && response.data.length > 0) {

            //var item = response.data[index];
            $scope.history = response.data;

            $scope.$broadcast('scroll.infiniteScrollComplete');
          } else {
            $scope.moreHistory = false;
          }
        }
      ).catch(function () {
        $scope.moreHistory = false;
        $ionicPopup.alert({title: $scope.ERR, template: "{{'ERR_CONNECTION' | translate}}"});
      });
    };

  });
})();
