(function(){
  controllers.controller('EstablishmentCtrl',
    ['$scope', '$ionicPopup', 'EstablishmentService', '$state', '$stateParams', '$translate', 'toastr', 'clipboard',
      function ($scope, $ionicPopup, EstablishmentService, $state, $stateParams, $translate, toastr, clipboard) {

        $scope.RANK = EstablishmentService.RANK;
        $scope.establishment = $stateParams.establishment;

        $translate('ERR').then(function (err) {
          $scope.ERR = err;
        });

        $translate('PHONE_COPIED').then(function (phone_cpd) {
          $scope.PHONE_COPIED = phone_cpd;
        });

        $translate('COPY_NOT_SUPPORTED').then(function (cpy_not_sup) {
          $scope.COPY_NOT_SUPPORTED = cpy_not_sup;
        });

        if (!clipboard.supported) {
            toastr.warning($scope.COPY_NOT_SUPPORTED);
        }

        $scope.clickHandler = function () {
            clipboard.copyText($scope.establishment.phones[0]);
            toastr.info($scope.PHONE_COPIED);
        };

        $scope.openEstablishmentPromotions = function(id, token) {
          EstablishmentService.searchEstablishmentPromotions(id, token, 0, 10).then( function(result){
            $state.go("establishment_promotions", {items: result.data, establishment: $scope.establishment});
          }, function(){
              toastr.warning($scope.ERR);
            //$ionicPopup.alert({title: "Deu um erro! (trocar essa mensagem!)"})
          });
        };

        // function calculateDiscount(oldPrice, price){
        //   var difference = oldPrice - price;
        //   var percent = (100* difference) / oldPrice;
        //   return percent.toFixed(1);
        // };
      }
    ]
  );
})();
