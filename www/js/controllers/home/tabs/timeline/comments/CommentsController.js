/**
 * Created by Bruno Rafal on 30/04/2016.
 */
(function(){
  controllers.controller('CommentsCtrl',
    function($rootScope, $scope, $ionicPopup, $ionicModal, $ionicPopover,
             $translate, PromotionService, PromotionSocket, toastr){

    const FIRST_POSITION = 0;

    var commentSelected;

    $scope.comments = [];
    $scope.moreComments = true;

    //comments

    $translate('ERR').then(function (err) {
      $ionicPopup.ERR = err;
    });
    $translate('COMMENT_NOT_FOUND').then(function (err) {
      $ionicPopup.COMMENT_NOT_FOUND = err;
    });

    function registerCommentListeners(promotion_id){
      PromotionSocket.registerNewCommentsAddedListener(promotion_id, function(json){
        $scope.comments.unshift(json.data);
        $scope.$applyAsync();
      });

      PromotionSocket.registerCommentsRemovedListener(promotion_id, function(json){
        $scope.comments.forEach(function(c, index) {
          if (c._id === json.data._id) {
            $scope.comments.splice(index, 1);
          }
        });
        $scope.$applyAsync();
      });

      PromotionSocket.registerCommentsUpdatedListener(promotion_id, function(json){
        $scope.comments.forEach(function(c, index) {
          if (c._id === json.data._id) {
            $scope.comments[index] = json.data;
          }
        });
        $scope.$applyAsync();
      });
    }

    function registerModalListeners(promotion_id){
      //Cleanup the modal when we're done with it!
      $scope.$on('$destroy', function() {
        $scope.modal.remove();
      });

      // Execute action on hide modal
      $scope.$on('modal.hidden', function() {
        $scope.comments = [];
        PromotionSocket.removeNewCommentsAddedListener($scope.promotionSelected._id);
      });

      // Execute action on remove modal
      $scope.$on('modal.removed', function() {
        // Execute action
      });

      registerCommentListeners(promotion_id);
    }

    $scope.openCommentsModal = function(promotion) {

      $scope.promotionSelected = promotion;

      $ionicPopover.fromTemplateUrl('view/templates/comments-popover.html', {
        scope: $scope
      }).then(function(popover) {
        $scope.popover = popover;
      });

      $ionicModal.fromTemplateUrl('view/templates/comments-modal.html',
        {
          scope: $scope,
          animation: 'slide-in-up'
        }
      ).then(function(modal) {
        $scope.modal = modal;
        $scope.moreComments = true;
        $scope.modal.show();
      });
      registerModalListeners(promotion._id);
    };

    $scope.addComment = function(text, loggedUser) {
      if (text != "" && typeof text != 'undefined') {
        var date = new Date();
        $("#comment-area").val("");
        var comment = {
          date: date,
          text: text,
          _user: loggedUser,
          _promotion: $scope.promotionSelected._id
        };
        PromotionSocket.addComment(comment, loggedUser.token, function(comment){
          $scope.comments.unshift(comment);
          $scope.$applyAsync();
        }, function(data){
           toastr.error($ionicPopup.ERR);
        });
      }
    };

    $scope.loadOldComments = function(skip, limit, token){
      PromotionService.loadOldComments(skip, limit, $scope.promotionSelected._id, token).then(
        function(json){
          var result = json.data;
          $rootScope.tokenInvalid(json);
          if(result.data && result.data.length > 0) {
            $scope.moreComments = true;
            for(var index = 0, len = result.data.length;  index < len; index++){
              $scope.comments.push(result.data[index]);
            }

            $scope.$broadcast('scroll.infiniteScrollComplete');
          }else{
            $scope.moreComments = false;
          }
        }
      ).catch(function(){
        $scope.moreComments = false;
        $ionicPopup.alert({title: $ionicPopup.ERR, template: "{{'ERR_CONNECTION' | translate}}"});
      });

      $scope.loadNewComments = function(promotionId, limit, token){
        var lastCommentId;
        if($scope.comments && $scope.comments[FIRST_POSITION]){
          lastCommentId = $scope.comments[FIRST_POSITION]._id;
        }
        PromotionService.loadNewComments(promotionId, lastCommentId, token).then(
          function(json){
            $rootScope.tokenInvalid(json);
            for(var index in json.data){
              $scope.comments.unshift(json.data[index]);
            }

            $scope.$broadcast('scroll.refreshComplete');
            $scope.$broadcast('scroll.refreshComplete');

          }).catch(function(){
            $ionicPopup.alert({template: $ionicPopup.COMMENT_NOT_FOUND}); //Replace to alert ionicPopup
            $scope.$broadcast('scroll.refreshComplete');
            $scope.$broadcast('scroll.refreshComplete');
          }
        );
      };

      $scope.removeComment = function (token){
        $scope.popover.hide();
        $ionicPopup.confirm({
          title: $translate.instant('REMOVE_COMMENT_POPUP_TITLE'),
          template: $translate.instant('REMOVE_COMMENT_POPUP_BODY')
        }).then(function(response){
          if(response){
            PromotionSocket.removeComment(token, commentSelected,
            function(){
              $scope.comments.splice($scope.comments.indexOf(commentSelected), 1);
              $scope.$applyAsync();
            },
            function(){
              toastr.error($ionicPopup.ERR);
            });
          }
        });
      };

      $scope.openCommentPopover = function (event, comment){
        commentSelected = comment;
        if(!$scope.popover.isShown()) {
          $scope.popover.show(event);
        } else {
          $scope.popover.hide();
        }
      };

      $scope.saveEditComment = function(){
        $scope.openHideEditComment();
        PromotionSocket.updateComment(commentSelected, $rootScope.loggedUser.token);
      };

      $scope.openHideEditComment = function(){
        $scope.popover.hide();
        commentSelected.edit = !commentSelected.edit;
      };
    };
  });
})();
