
/**
 * Created by Bruno Rafael on 08/02/2016.
 */
(function(){
  controllers.controller('TimelineCtrl',
    function($scope, $state, $rootScope, $ionicPopup, $timeout, $translate, PromotionService, FavoriteService,
             EstablishmentService, PromotionSocket, WebSocketService, toastr) {

      $scope.RANK = EstablishmentService.RANK;
      $translate('ERR').then(function (err) {
        $scope.ERR = err;
      });
      $translate('FAVORITE_ADDED').then(function (fav_add) {
        $scope.FAVORITE_ADDED = fav_add;
      });
      $translate('FAVORITE_REMOVED').then(function (fav_rem) {
        $scope.FAVORITE_REMOVED = fav_rem;
      });
      $translate('ESTABLISHMENT_NOT_FOUND').then(function (establishment_not_found) {
        $scope.ESTABLISHMENT_NOT_FOUND = establishment_not_found;
      });

      const FIRST_POSITION = 0;

      $scope.items = [];
      $scope.moreData = true;

      function updatePromotion(json) {
        var id = json.promotion_id;
        for (var index in $scope.items) {
          if ($scope.items[index]._id === id) {
            $scope.items[index].likes = json.likes
          }
        }
        $scope.$applyAsync();
      }

      $scope.calculateDiscount = function(oldPrice, price) {
        var difference = oldPrice - price;
        var percent = (100 * difference) / oldPrice;
        if(Number.isInteger(percent)){
          return percent;
        }
        return parseFloat(percent.toFixed(1));
      };

      $scope.dateFromObjectId = function(objectId) {
         var id = objectId;
	       return parseInt(id.substring(0, 8), 16) * 1000;
       };

      $scope.loadOldData = function (skip, limit, removeFinalizedPromotions, token) {
        var params = {
          skip: skip,
          limit: limit + skip,
          removeFinalizedPromotions: removeFinalizedPromotions,
          token: token
        };
        PromotionService.loadOldPromotions(params).then(
          function (json) {
            $rootScope.tokenInvalid(json);
            if (json.data && json.data.length > 0) {
              $scope.moreData = true;
              for (var index in json.data) {
                var item = json.data[index];
                item.discount = $scope.calculateDiscount(item.price.old, item.price.actual);
                $scope.items.push(item);
              }
              $scope.$broadcast('scroll.infiniteScrollComplete');
            } else {
              $scope.moreData = false;
            }
          }
        ).catch(function () {
          $scope.moreData = false;
          toastr.error($scope.ERR_CONNECTION, scope.ERR);
          //$ionicPopup.alert({title: $scope.ERR, template: "{{'ERR_CONNECTION' | translate}}"});
        });
      };

      $scope.loadNewData = function (limit, removeFinalizedPromotions, establishmentId) {
        var lastPromotionAddedId = $scope.items.length !== 0?$scope.items[FIRST_POSITION]._id : undefined;
        var params = {
          lastPromotionAddedId: lastPromotionAddedId,
          limit: limit,
          establishmentId: establishmentId,
          removeFinalizedPromotions: removeFinalizedPromotions,
          token: $rootScope.loggedUser.token
        };

        PromotionService.loadNewPromotions(params).then(
          function (json) {
            if (json.data.length) {
              $rootScope.tokenInvalid(json);
              for (var index in json.data) {
                var item = json.data[index];
                item.discount = $scope.calculateDiscount(item.price.old, item.price.actual);
                $scope.items.unshift(item);
              }
            }

            $scope.$broadcast('scroll.refreshComplete');
            $scope.$broadcast('scroll.refreshComplete');

          }).catch(function () {
            toastr.warning($scope.PROMOTIONS_NOT_FOUND);
          //$ionicPopup.alert({template: "{{'PROMOTIONS_NOT_FOUND' | translate}}"});
          $scope.$broadcast('scroll.refreshComplete');
          $scope.$broadcast('scroll.refreshComplete');
        });

      };

      $scope.addOrRemoveLike = function (promotion) {
        if (!$scope.likeBlocked) {
          function reject() {
            $scope.likeBlocked = false;
            toastr.error($scope.ERR_PROMOTION_EVALUATE);
            //$ionicPopup.alert({template: "{{'ERR_PROMOTION_EVALUATE' | translate}}"});
          }

          function resolve(){
            $timeout(function(){
              $scope.likeBlocked = false;
            }, 300);
            promotion.like = !promotion.like;
            promotion.likes = promotion.like ? promotion.likes + 1 : promotion.likes - 1;
            $scope.$applyAsync();
          }

          $scope.likeBlocked = true;
          if (!promotion.like) {
            PromotionSocket.addLike(promotion._id, resolve, reject);
          } else {
            PromotionSocket.removeLike(promotion._id, resolve, reject);
          }
        }
      };

      $scope.addFavorite = function(item){

        FavoriteService.addFavorite($rootScope.loggedUser.token, item._id, item.productType).then(
          function(response){
            var json = response.data;
            if(json.success){
              item.favorite = !item.favorite;
              $scope.$applyAsync();
              toastr.success($scope.FAVORITE_ADDED);
            } else {
              //console.error(json.data);
            }
          }
        ).catch(
          function(exception){
            //console.error(exception);
          }
        );

      };

      $scope.removeFavorite = function(item){

        FavoriteService.removeFavorite($rootScope.loggedUser.token, item._id).then(
          function(response){
            var json = response.data;
            if(json.success){
              item.favorite = !item.favorite;
              $scope.$applyAsync();
              toastr.success($scope.FAVORITE_REMOVED);
            } else {
              //console.error(json.data);
            }
          }
        ).catch(
          function(exception){
            //console.error(exception);
          }
        );
      };

      $rootScope.$on('timelineUpdate', function(event, args){
        $rootScope.loggedUser.removeFinishPromotions = args[1];
        $scope.items = [];
        for (var index in args[0].data) {
          var item = args[0].data[index];
          item.discount = $scope.calculateDiscount(item.price.old, item.price.actual);
          $scope.items.push(item);
        }
        $scope.$applyAsync();
      });

      function registerListeners() {
        PromotionSocket.addUpdateLikesListener(updatePromotion);
      }
      registerListeners();

      }
  );
})();
