(function() {
   controllers.controller('SettingsCtrl',
     ['$scope', '$state', '$rootScope', '$translate', '$window', '$ionicPopup', 'UserService', 'toastr',
       function($scope, $state, $rootScope, $translate, $window, $ionicPopup, UserService, toastr){

         $scope.removeFinishPromotions = $rootScope.loggedUser.settings.removeFinishPromotions;
         var SUCCESS = 200;

         const MIN_PASSWORD_LENGTH = 6;

         $scope.back = function(){
           $state.go('tab.timeline');
         };

         $translate('INVALID_EMAIL').then(function (inv_email) {
           $scope.INVALID_EMAIL = inv_email;
         });

         $translate('ERR_EDIT_NAME').then(function (err_ename) {
           $scope.ERR_EDIT_NAME = err_ename;
         });

         $translate('INVALID_CHARACTERES').then(function (inv_char) {
           $scope.INVALID_CHARACTERES = inv_char;
         });

         $translate('PASS_DO_NOT_MATCH').then(function (psw_dont_match) {
           $scope.PASS_DO_NOT_MATCH = psw_dont_match;
         });

         $translate('PASSWORD_SIZE').then(function (psw_size) {
           $scope.PASSWORD_SIZE = psw_size;
         });

         $translate('ERR_EDIT_PHONE').then(function (err_phone) {
           $scope.ERR_EDIT_PHONE = err_phone;
         });

         $translate('INVALID_PHONE').then(function (err_phone) {
           $scope.INVALID_PHONE = err_phone;
         });

         $scope.changeLanguage = function(lang){
		       $translate.use(lang);
           UserService.updateLanguage($rootScope.loggedUser._id, lang, $rootScope.loggedUser.token).then(function(json){
             if (json.status != SUCCESS) {
               toastr.error($scope.ERR_CONNECTION, $scope.ERR);
               //$ionicPopup.alert({title: $scope.ERR, template: "{{'ERR_CONNECTION' | translate}}"});
             }
           }).catch(function(exception){
             toastr.error($scope.ERR_CONNECTION, $scope.ERR);
             //$ionicPopup.alert({title: $scope.ERR, template: "{{'ERR_CONNECTION' | translate}}"});
           });
	       };

         $scope.removeAllFinalizedPromotions = function(flag){
           UserService.updateFinalizedPromotions(flag, $rootScope.loggedUser.token).then(
             function(result){
                $rootScope.$broadcast('timelineUpdate', [result, flag]);
                $rootScope.loggedUser.settings.removeFinishPromotions = flag;
             }
           ).catch(
             function(){
                toastr.error($scope.ERR);
             }
           );

         };

         $scope.updateVibration = function(flag){
           UserService.updateVibration(flag, $rootScope.loggedUser.token).then(function(result){
             if(result.status == SUCCESS){
                 $rootScope.loggedUser.settings.vibration = flag;
                 $rootScope.sharedPreferences.getSharedPreferences('loggedUserPreferences', 'MODE_PRIVATE',
                     function(){
                         $rootScope.sharedPreferences.putString('usuarioLogado',  JSON.stringify($rootScope.loggedUser));
                     },
                     function(err){
                         console.error("Get shared preferences error");
                     }
                 );
             }
           });
         };

         $scope.language = {
           items: [
             {
               name: 'Português (Brasil)',
               val: 'pt-br'
             },
             {
               name: 'English (USA)',
               val: 'en-us'
             },
             {
               name: 'Español (España)',
               val: 'es'
             }
           ],
           show: false
         };

         $scope.edit = {
           name: {show: false},
           email: {show: false},
           phone: {show: false},
           password: {show: false}
         };

         $scope.data = {
           clientSide: $translate.proposedLanguage()
         };

         $scope.toggleGroup = function(group) {
           group.show = !group.show;
         };

         $scope.isGroupShown = function(group) {
           return group.show;
         };

         function validateName(name) {
           return /^[a-z\u00C0-\u00ff A-Z]+$/.test(name) && !/^\s+$/.test(name);
         }

         function validatePhone(phone) {
           return /^\(?\d{2}\)?[\s-]?[\s9]?\d{4,5}-?\d{4}$/.test(phone);
         }

         $scope.firstName = function () {
           var name = $rootScope.loggedUser.name;
           var firstName = name.split(" ");
           return firstName[0];
         }

         $scope.lastName = function () {
           var name = $rootScope.loggedUser.name;
           var lastName = name.split(" ");
           if (lastName.length > 1) {
             var lname = "";
             for (var i = 1; i < lastName.length; i++) {
               lname += lastName[i] + " ";
             }
             return lname;
           } else {
             return "";
           }
         }

         function validateEmail(email) {
            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
         }

         $scope.updateEmailUser = function() {
           var email = document.getElementById("newEmail").value;
           if(validateEmail(email)) {
             UserService.updateEmail($rootScope.loggedUser._id, email, $rootScope.loggedUser.token).then(function(json){
               if (json.status === SUCCESS) {
                 $scope.toggleGroup($scope.edit.email);
                 $rootScope.loggedUser.email = email;
               } else {
                 toastr.error($scope.ERR_EDIT_EMAIL, $scope.ERR);
              //$ionicPopup.alert({title: $scope.ERR, template: "{{'ERR_EDIT_EMAIL' | translate}}"});
              }
            }).catch(function(exception){
              toastr.error($scope.ERR_CONNECTION, $scope.ERR);
              //$ionicPopup.alert({title: $scope.ERR, template: "{{'ERR_CONNECTION' | translate}}"});
            });
          } else {
             clear(document.getElementById("newEmail"));
             toastr.error($scope.INVALID_EMAIL, $scope.ERR);
          }
         };

         $scope.updatePassword = function() {
           var password = document.getElementById("password").value;
           var newPassword = document.getElementById("newPassword").value;
           var confirmPassword = document.getElementById("confimNewPassword").value;
           if (newPassword == confirmPassword && newPassword.length >= MIN_PASSWORD_LENGTH) {
             UserService.updatePassword($rootScope.loggedUser._id, password, newPassword, $rootScope.loggedUser.token).then(function (json) {
               if (json.status === SUCCESS) {
                 $scope.toggleGroup($scope.edit.password);
               } else {
                 toastr.error($scope.ERR_EDIT_PASSWORD, $scope.ERR);
                 //$ionicPopup.alert({title: $scope.ERR, template: "{{'ERR_EDIT_PASSWORD' | translate}}"});
               }
             }).catch(function (exception) {
               toastr.error($scope.ERR_CONNECTION, $scope.ERR);
               //$ionicPopup.alert({title: $scope.ERR, template: "{{'ERR_CONNECTION' | translate}}"});
             });
           } else {
             if(newPassword.length <= 5) {
               toastr.error($scope.PASSWORD_SIZE, $scope.ERR);
             } else {
               toastr.error($scope.PASS_DO_NOT_MATCH, $scope.ERR);
             }
             //$ionicPopup.alert({title: $scope.ERR, template: "{{'PASS_DO_NOT_MATCH' | translate}}"});
           }
         }

         $scope.updatePhone = function() {
           var phone = document.getElementById("newPhone").value;
           if(validatePhone(phone)) {
            UserService.updatePhone($rootScope.loggedUser._id, phone, $rootScope.loggedUser.token).then(function(json){
             if (json.status === SUCCESS) {
               $scope.toggleGroup($scope.edit.phone);
             } else {
               toastr.error($scope.ERR_EDIT_PHONE, $scope.ERR);
               //$ionicPopup.alert({title: $scope.ERR, template: "{{'ERR_EDIT_PHONE' | translate}}"});
              }
            }).catch(function(exception){
             toastr.error($scope.ERR_CONNECTION, $scope.ERR);
             //$ionicPopup.alert({title: $scope.ERR, template: "{{'ERR_CONNECTION' | translate}}"});
            });
          } else {
             clear(document.getElementById("newPhone"));
             toastr.error($scope.INVALID_PHONE, $scope.ERR);
          }
        }

         $scope.updateNameUser = function() {
           var newName = document.getElementById("newName").value
           var newLastName = document.getElementById("newLastName").value
           var name = newName + " " + newLastName;
           if(validateName(name)) {
            UserService.updateName($rootScope.loggedUser._id, name, $rootScope.loggedUser.token).then(function(json){
             if (json) {
              if (json.status === SUCCESS) {
                $scope.toggleGroup($scope.edit.name);
                $rootScope.loggedUser.name = name;
              } else {
                toastr.error($scope.ERR_EDIT_NAME, $scope.ERR);
              //  $ionicPopup.alert({title: $scope.ERR, template: "{{'ERR_EDIT_NAME' | translate}}"});
              }
             }
            }).catch(function(exception){
             toastr.error($scope.ERR_CONNECTION, $scope.ERR);
          //  $ionicPopup.alert({title: $scope.ERR, template: "{{'ERR_CONNECTION' | translate}}"});
            });
          } else {
              clear(document.getElementById("newName"));
              toastr.error($scope.INVALID_CHARACTERES, $scope.ERR);
          }
         };

         function clear(field) {
           field.value = '';
         }

         $scope.focus = function(campo) {
           campo.$focus=true;
           campo.$blur=false;
         };

         $scope.blur = function(campo) {
           campo.$focus=false;
           campo.$blur=true;
         };
       }
     ]
   );
 })();
