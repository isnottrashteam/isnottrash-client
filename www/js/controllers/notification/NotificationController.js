/**
 * Created by Bruno Rafael on 10/06/2016.
 */
(function(){
  controllers.controller('NotificationCtrl',
    function($scope, $rootScope, $translate, $ionicPopup, STATES, NotificationService, toastr){

      $scope.moreNotifications = true;

      $scope.loadOldNotifications = function (skip, limit, token) {
        var params = {
          skip: skip,
          limit: limit,
          token: token
        };
        NotificationService.loadOldNotifications(params).then(
          function (json) {
            var response = json.data;
            $rootScope.tokenInvalid(json);
            if (response && response.data && response.data.length > 0) {
              $scope.moreNotifications = true;
              for(var i in response.data){
                $rootScope.notifications.push(response.data[i]);
              }
              $scope.$broadcast('scroll.infiniteScrollComplete');
            } else {
              $scope.moreNotifications = false;
            }
          }
        ).catch(function () {
          $scope.moreNotifications = false
          toastr.error($scope.ERR_CONNECTION, $scope.ERR);
          //$ionicPopup.alert({title: $scope.ERR, template: "{{'ERR_CONNECTION' | translate}}"});
        });
      };

      $scope.showHintPopup = function(hint){
        $scope.hint = hint;
        return $ionicPopup.show({
          templateUrl: "hint-popup.html",
          //title: $scope.promotion.productName,
          scope: $scope,
          cssClass: 'hint-popup',
          buttons: [
            { text: $translate.instant('CLOSE'), type: 'button-positive' }
          ]
        });
      };

      $scope.visualizeNotification = function(not, token){

        NotificationService.visualizeNotifications({ids: [not._id], token: token}).then(
          function(){
            $rootScope.goTo(STATES.PROMOTION_DETAILS, {item: not.info});
          }
        );
      }
    }
  );
})();
