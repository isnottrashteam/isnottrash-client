// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in all controllers
// 'starter.services' is found in all services

(function(){
app.run(function($ionicPlatform, $ionicHistory, $rootScope, $state, $window, $ionicLoading, WebSocketService, EVENTS) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins){
      // Android customization
      //cordova.plugins.backgroundMode.setDefaults({ text:'Doing heavy tasks.'});
      //// Enable background mode
      //cordova.plugins.backgroundMode.enable();
      //
      //// Called when background mode has been activated
      //cordova.plugins.backgroundMode.onactivate = function () {
      //  var config = { silent: false };
      //  setTimeout(function () {
      //    if(cordova.plugins.backgroundMode.isActive()){
      //      // Modify the currently displayed notification
      //      cordova.plugins.backgroundMode.configure(config);
      //    } else {
      //      config.text = 'Running in background for more than 5s now.';
      //      cordova.plugins.backgroundMode.configure(config);
      //    }
      //    // Modify the currently displayed notification
      //    cordova.plugins.backgroundMode.configure({
      //      text:'Running in background for more than 5s now.',
      //      silent: false
      //    });
      //  }, 5000);
      //};
      $rootScope.sharedPreferences = window.SharedPreferences;
      $rootScope.sharedPreferences.getSharedPreferences('loggedUserPreferences', 'MODE_PRIVATE',
        function(){
          $rootScope.sharedPreferences.getString('usuarioLogado',
              function(value){
                if(value){
                  var userData = JSON.parse(value);
                  if (userData) {
                    $rootScope.loggedUser = userData;
                    WebSocketService.connect($rootScope.loggedUser.token);
                    $rootScope.goTo('tab.timeline', userData);
                  }
                }
              },
              function(error) {
                if (error !== "No data") {
                  console.error("Get string error in shared preferences");
                }
              }
          );
        },
        function(){
          console.error("Get shared preferences error");
        });

      if(window.cordova.plugins.Keyboard) {
        //cordova.plugins.Keyboard.hideKeyboardAccepromodetailsssoryBar(true);
        cordova.plugins.Keyboard.disableScroll(true);
      }
    }

    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }

    $rootScope.notifications = [];

    $rootScope.$on(EVENTS.NOTIFICATION_ADDED, function(info, response){
      for(var i in response){
        $rootScope.notifications.unshift(response[i]);
        if($rootScope.loggedUser.settings.vibration) {
          navigator.vibrate(1000);
        }
      }
      $rootScope.$applyAsync();
    });

    $rootScope.goTo = function (state, params) {
      $state.go(state, params);
    };

    $rootScope.dateFromObjectId = function (objectId) {
      return new Date(parseInt(objectId.substring(0, 8), 16) * 1000);
    };

    $rootScope.back = function () {
      $ionicHistory.goBack(-1);
    };

    $rootScope.showHeader = function(){
      return $ionicHistory.currentStateName() !== 'settings' &&
        $ionicHistory.currentStateName() !== 'login' &&
        $ionicHistory.currentStateName() !== 'promotion_details' &&
        $ionicHistory.currentStateName() !== 'establishment_perfil' &&
        $ionicHistory.currentStateName() !== 'establishment_promotions';
    };

    $rootScope.tokenInvalid = function(req) {
      if (req.status === 403) {
        $window.location.reload(true);
        $rootScope.sharedPreferences.getSharedPreferences('loggedUserPreferences', 'MODE_PRIVATE', function(){
          $rootScope.sharedPreferences.remove('usuarioLogado');
          $ionicLoading.hide();
          $state.go('login');
        }).catch(function(){
            console.error("Shared preferences not getting");
        });
      }
    };

    $rootScope.sliceString = function (name){
      if(!name) return;
      var slice = name.split(' ');
      return slice[0] + ' ' + slice[slice.length - 1];
    };

    $rootScope.formatDate = function(pDate){
      var date = new Date(pDate);

      var day = date.getDate();
      if(day < 10){
        day = "0" + day;
      }

      var month = (date.getMonth() + 1);
      if(month < 10){
        month = "0" + month;
      }

      var year = date.getFullYear();
      return day + "/" + month +"/" + year;
    };
  });
})

.config(function($stateProvider, $urlRouterProvider, $httpProvider, $translateProvider) {

  for(var lang in translations){
    $translateProvider.translations(lang, translations[lang]);
  }

  $translateProvider.preferredLanguage('pt-br');
  $translateProvider.useSanitizeValueStrategy('escape');

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in modules.js

  $stateProvider
  .state('login', {
    url: '/login',
    cache: true,
    templateUrl: 'view/login/login.html',
    controller: 'LoginCtrl'
  })

  .state('settings', {
    url: '/settings',
    cache: true,
    templateUrl: 'view/settings/settings.html',
    controller: 'SettingsCtrl'
  })

  .state('promotion_details', {
    url: '/promotion_details',
    params: {'item': null},
    cache: false,
    templateUrl: 'view/home/tabs/timeline/promotion_details/promotion-details.html',
    controller: 'PromotionDetailsCtrl'
  })

  .state('tab', {
    url: '/tab',
    abstract: true,
    cache: true,
    templateUrl: 'view/home/tabs/tabs.html',
    controller: 'TabsCtrl'
  })

  .state('tab.timeline', {
    url: '/timeline',
    cache: true,
    views: {
      'tab-timeline': {
        templateUrl: 'view/home/tabs/timeline/tab-timeline.html',
        controller: 'TimelineCtrl'
      }
    }
  })
  .state('tab.search', {
      url: '/search',
    cache: true,
      views: {
        'tab-search': {
          templateUrl: 'view/home/tabs/search/tab-search.html',
          controller: 'SearchCtrl'
        }
      },

  })

  .state('tab.notification', {
    url: '/notifications',
    cache: true,
    views: {
      'tab-notification': {
        templateUrl: 'view/home/tabs/notifications/tab-notifications.html',
        controller: 'NotificationCtrl'
      }
    }
  })

  .state('establishment_perfil', {
    url: '/establishment',
    cache: true,
    params: {'establishment': null},
    templateUrl: 'view/home/tabs/timeline/tab-establishment.html',
    controller: 'EstablishmentCtrl',
  })

  .state('establishment_promotions', {
    url: '/establishment/promotions',
    cache: true,
    params: {establishment: null},
    templateUrl: 'view/home/tabs/timeline/establishment-promotions.html',
    controller: 'EstablishmentPromotionsCtrl'
  });
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/login');
});

  var compareTo = function() {
    return {
      require: "ngModel",
      scope: {
        otherModelValue: "=compareTo"
      },
      link: function(scope, element, attributes, ngModel) {
          ngModel.$validators.compareTo = function(modelValue) {
              return modelValue == scope.otherModelValue;
          };
          scope.$watch("otherModelValue", function() {
              ngModel.$validate();
            });
          }
        };
      };

  var uiTelefone = function(){
     return {
        require: 'ngModel',
        link: function(scope, element, attr, ctrl){
          var _formatTelefone = function(telefone){
            if(telefone===undefined){
              return;
            }
            //(99)9999-9999 - 13dig
            //(99)99999-9999 - 14dig
            telefone = telefone.replace(/[^0-9]+/g, "");
              if(telefone.length > 0){
                telefone = telefone.substring(-1,0) + "(" + telefone.substring(0);
              }
              if(telefone.length > 3){
                telefone = telefone.substring(0,3) + ")" + telefone.substring(3);
              }
              if(telefone.length == 12){
                telefone = telefone.substring(0,8) + "-" + telefone.substring(8);
              }else if(telefone.length >= 13){
                telefone = telefone.substring(0,9) + "-" + telefone.substring(9,13);
              }
              return telefone;
          }

          element.bind('keyup', function(){
            ctrl.$setViewValue(_formatTelefone(ctrl.$viewValue));
            ctrl.$render();
          });
        }
    };
  }

app.directive("compareTo", compareTo);
app.directive("uiTelefone", uiTelefone);
})();
